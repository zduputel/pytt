'''
A class that deals with travel-time and seismic data windowing

Written by Z. Duputel, November 2013
'''

# Externals
from copy    import deepcopy
from math    import sin, cos, acos, pi
from os.path import exists

class pytt(object):
    
    def __init__(self,travel_time_table,win=None):
        '''
        Args:
            * travel_time_table: filename of travel_time_table
        '''
        
        assert exists(travel_time_table), 'Cannot read %s (no such file)'%(travel_time_table)
        
        # Initialize W-phase window parameters
        self.win  = win
        self.win4 = None
        if self.win !=None:
            self.decodeWin()

        # Read travel time table
        f = open(travel_time_table, 'r')
        line  = f.readline().split()
        self.nh    = len(line)-1
        self.hv    = []
        self.P_lut = []
        for j in range(self.nh):
            self.hv.append(float(line[j+1]))
            self.P_lut.append([])

        self.dv    = []
        while( True ):
            line = f.readline().split()
            if(len(line) > 0):
                self.dv.append(float(line[0]))
                for j in range(self.nh):
                    self.P_lut[j].append(float(line[j+1]))
            else:
                break
        f.close()
        self.nd = len(self.dv)
        
        # All done
        return

    def calcDistSphere(self,evla, evlo, stla, stlo):
        '''
        Return epicentral distance on a sphere
        Args:
            * evla: event latitude
            * evla: event longitude
            * stla: station latitude
            * stlo: station longitude
        '''

        DR = pi/180.
        cosang = sin((evla)*DR)*sin((stla)*DR)+ \
                 cos((evla)*DR)*cos((stla)*DR)*cos((evlo-stlo)*DR)
        gcarc  = acos(cosang)/DR

        # All done
        return gcarc

    def calcPttFromDist(self,h, d):
        '''
        Return P-wave travel-time for a given depth and distance
        Args:
            * h: hypocenter depth (km)
            * d: epicentral distance (km)
        '''
        hi = None
        for j in range(self.nh-1):
            if self.hv[j] <= h and h <= self.hv[j+1]:
                hi = j
                break
        if hi == None:
            hi = self.nh-2
        rh = (h - self.hv[hi])/(self.hv[hi+1]-self.hv[hi])
        
        di = None
        for k in range(self.nd-1):
            if self.dv[k] <= d and d <= self.dv[k+1]:
                di = k
                break
        if di == None:
            di = self.nd-2
        rd = (d - self.dv[di])/(self.dv[di+1]-self.dv[di])

        t1 = self.P_lut[hi][di  ]*rh + self.P_lut[hi+1][di  ]*(1.-rh)
        t2 = self.P_lut[hi][di+1]*rh + self.P_lut[hi+1][di+1]*(1.-rh)
        P_tt = t1*rd + t2*(1.-rd)

        # All done
        return P_tt
        
    def calcPttFromLatLon(self,depth,evla,evlo,stla,stlo):
        '''
        Return P-wave travel-time for a given source and station location
        Args:
            * depth: event depth
            * evla:  event latitude
            * evla:  event longitude
            * stla:  station latitude
            * stlo:  station longitude
        '''
            
        gcarc = self.calcDistSphere(evla, evlo, stla, stlo)
        P_tt = self.calcPttFromDist(depth,gcarc)
        
        # All done
        return P_tt

    def decodeWin(self):
        '''
        Assign win4 parameter for time-window parameters
        '''

        # Check win
        np = len(self.win)
        assert np >= 1 or np <= 4, 'Error in wphase window parameters'

        # Initialize win4
	self.win4 = [0.,0.,0.,180.]

        if np == 1:
            self.win4[1] =   self.win[0];

        elif np == 2:
            self.win4[0] = self.win[0];
            self.win4[1] = self.win[1];
        elif np == 3:
            self.win4[0] = self.win[0];
            self.win4[1] = self.win[1];
            self.win4[2] = self.win[2];
        else:
            self.win4 = deepcopy(self.win)
            
        # All done
        return

    def windowFromLatLon(self,depth,evla,evlo,stla,stlo,o_distance=False):
        '''
        return the W-phase window
        Args:
            * depth: event depth
            * evla:  event latitude
            * evla:  event longitude
            * stla:  station latitude
            * stlo:  station longitude
        '''
        
        # Check win4
        assert self.win4 != None, 'win4 parameters must be assigned'

        # Compute epicentral distance (in deg)
        gcarc = self.calcDistSphere(evla, evlo, stla, stlo)
        
        # Time-window business
        if self.win4[2] > gcarc:
            feakdist = self.win4[2] 
        else:
            feakdist = gcarc
        
        if (self.win4[3] < feakdist):
            feakdist = self.win4[3] 

        twp_beg = self.win4[0] * feakdist 
        twp_end = self.win4[1] * feakdist 

        # Add the P-wave travel-time 
        P_tt = self.calcPttFromDist(depth,gcarc)
        twp_beg += P_tt
        twp_end += P_tt

        # All done
        if o_distance:
            return twp_beg,twp_end,gcarc
        return twp_beg,twp_end
